const rainyContainer = document.getElementById("rainy-container");
const maxRaindrops = 50;

function createRaindrop() {
    // Check the number of existing raindrops
    const existingRaindrops = document.getElementsByClassName("raindrop").length;

    // Create a new raindrop only if the limit is not reached
    if (existingRaindrops < maxRaindrops) {
        // Create a new raindrop element
        const raindrop = document.createElement("div");
        raindrop.classList.add("raindrop");

        // Generate a random number between 0 and the width of the container
        const x = Math.floor(Math.random() * rainyContainer.offsetWidth);

        // Set the position of the raindrop using the generated x value
        raindrop.style.left = `${x}px`;
        raindrop.style.animationDuration = ( 0.75 + Math.random() * 0.25 ) + 's';

        // Add the raindrop to the container
        rainyContainer.appendChild(raindrop);
    } else {
        // Remove excess raindrops if the limit is reached
        const excessRaindrops = existingRaindrops - maxRaindrops;
        for (let i = 0; i < excessRaindrops; i++) {
            rainyContainer.removeChild(rainyContainer.firstChild);
        }
    }
}

// Create a new raindrop every 55 milliseconds
setInterval(createRaindrop, 400);
